<?php

declare(strict_types = 1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\Models\Category;
use Illuminate\Support\Carbon;

/**
 * Class Task
 *
 * @package App\Models
 */
class Task extends Model
{
    /**
     * @var string
     */
    protected $table = 'tasks';

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'due_date',
        'status',
        'category_id',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'title'       => 'string',
        'due_date'    => 'string ',
        'status'      => 'bool',
        'category_id' => 'int',
    ];

    /**
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @param null|bool $status
     *
     * @return void
     */
    public function setStatusAttribute($status): void
    {
        $this->attributes['status'] = (is_null($status)) ? 0 : 1;
    }
    /**
     * @param string $dueDate
     *
     * @return void
     */
    public function setDueDateAttribute(string $dueDate): void
    {
        $this->attributes['due_date'] = Carbon::make($dueDate);
    }

    /**
     * @param string $dueDate
     *
     * @return string
     */
    public function getDueDateAttribute(string $dueDate): string
    {
        return Carbon::make($dueDate)->format('m/d/Y');
    }
}
