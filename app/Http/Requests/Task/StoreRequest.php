<?php

declare(strict_types = 1);

namespace App\Http\Requests\Task;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreRequest
 *
 * @package App\Http\Requests\Task
 */
class StoreRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => [
                'required',
                'max:50',
                'min:2',
                'string',
            ],
            'due_date' => [
                'required',
                'date',
                'after:today',
            ],
            'status' => [
                'boolean',
            ],
            'category_id' => [
                'required',
                'numeric',
                'exists:categories,id',
            ],
        ];
    }
}
