<?php

declare(strict_types = 1);

namespace App\Http\Controllers;

use App\Http\Requests\Task\StoreRequest;
use App\Http\Requests\Task\UpdateRequest;
use App\Models\Category;
use App\Models\Task;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Illuminate\Contracts\View\View as ViewContract;

/**
 * Class TaskController
 *
 * @package App\Http\Controllers
 */
class TaskController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function index(): ViewContract
    {
        $tasks = Task::orderBy('created_at', 'desc')->paginate(10);

        return View::make('tasks.index', [
            'tasks' => $tasks,
        ]);
    }

    /**
     * @param \App\Models\Task $task
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function show(Task $task): ViewContract
    {
        return View::make('tasks.show', [
            'task' => $task,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function create(): ViewContract
    {
        $categories = Category::all();

        return View::make('tasks.create', [
            'categories' => $categories,
        ]);
    }

    /**
     * @param \App\Http\Requests\Task\StoreRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request): RedirectResponse
    {
        Task::create($request->all([
            'title',
            'due_date',
            'category_id',
        ]));

        return Redirect::route('tasks.index');
    }

    /**
     * @param \App\Models\Task $task
     *
     * @return ViewContract
     */
    public function edit(Task $task): ViewContract
    {
        $categories = Category::all();

        return View::make('tasks.edit', [
            'task'       => $task,
            'categories' => $categories,
        ]);
    }

    /**
     * @param \App\Http\Requests\Task\UpdateRequest $request
     * @param \App\Models\Task $task
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, Task $task): RedirectResponse
    {
        $task->update($request->all([
            'title',
            'due_date',
            'status',
            'category_id',
        ]));

        return Redirect::route('tasks.index');
    }

    /**
     * @param \App\Models\Task $task
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Task $task): RedirectResponse
    {
        $task->delete();

        return Redirect::route('tasks.index');
    }
}
