<?php

declare(strict_types = 1);

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Config;

/**
 * Class DevelopmentServiceProvider
 *
 * @package App\Providers
 */
class DevelopmentServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    protected $providers = [
        \Barryvdh\Debugbar\ServiceProvider::class,
        \Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class,
    ];

    /**
     * @return void
     */
    public function boot(): void
    {
        if (!Config::get('app.debug')) {
            return;
        }

        foreach ($this->providers as $provider) {
            $this->app->register($provider);
        }
    }
}
