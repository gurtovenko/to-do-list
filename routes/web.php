<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::get('/', 'HomeController@index')->name('home')->middleware('verified');

Route::group(['prefix' => 'tasks', 'as' => 'tasks', 'middleware' => 'verified'], __DIR__ . '/web/tasks.php');

Route::group(
    [
        'prefix'     => 'categories',
        'as'         => 'categories',
        'middleware' => 'verified'
    ],
    __DIR__ . '/web/categories.php'
);
