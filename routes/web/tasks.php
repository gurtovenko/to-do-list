<?php

declare(strict_types = 1);

Route::get('/', 'TaskController@index')->name('.index');
Route::post('/', 'TaskController@store')->name('.store');
Route::patch('{task}', 'TaskController@update')->name('.update');
Route::get('{task}/edit', 'TaskController@edit')->name('.edit');
Route::get('create', 'TaskController@create')->name('.create');
Route::delete('{task}', 'TaskController@destroy')->name('.destroy');
