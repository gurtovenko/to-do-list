<?php

declare(strict_types = 1);

Route::get('/', 'CategoryController@index')->name('.index');
Route::get('{category}/edit', 'CategoryController@edit')->name('.edit');
Route::patch('{category}', 'CategoryController@update')->name('.update');
Route::get('create', 'CategoryController@create')->name('.create');
Route::post('/', 'CategoryController@store')->name('.store');
Route::delete('{category}', 'CategoryController@destroy')->name('.destroy');
