<?php

declare(strict_types = 1);

return [
    'heading' => 'To do list',
    'create'  => 'Create new task',
    'form'    => [
        'title'    => 'Title',
        'dueDate'  => 'Due date',
        'category' => 'Category',
        'select'   => 'Select a category',
        'done'     => 'Done',
    ],
    'button' => [
        'add'    => 'Add item',
        'update' => 'Update',
        'submit' => 'Submit',
    ],
];
