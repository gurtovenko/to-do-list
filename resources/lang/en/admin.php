<?php

declare(strict_types=1);

return [
    'dashboard' => [
        'heading' => 'Dashboard',
    ],
    'header' => [
        'task' => [
            'notify'  => 'You have a new task|You have new tasks',
            'viewAll' => 'View all tasks',
        ],
        'user' => [
            'since'   => 'Member since',
            'profile' => 'Profile',
            'signOut' => 'Sign out',
        ],
    ],
    'sidebar' => [
        'search'     => 'Search',
        'heading'    => 'Header',
        'tasks'      => 'Tasks',
        'categories' => 'Categories',
    ],
    'control' => [
        'recent'   => 'Recent Activity',
        'progress' => 'Tasks Progress',
        'settings' => 'General Settings',
    ],
    'footer' => [
        'copyright' => 'Copyright',
        'rights'    => 'All rights reserved.',
    ],
];
