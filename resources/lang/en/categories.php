<?php

declare(strict_types = 1);

return [
    'heading' => 'Categories',
    'create'  => 'Add new category',
    'edit'    => 'Edit',
    'form'    => [
        'title' => 'Title',
    ],
    'button'  => [
        'update' => 'Update',
        'edit'   => 'Edit',
        'create' => 'Create',
        'submit' => 'Submit',
    ],
];
