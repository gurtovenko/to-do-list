@extends('layouts.admin')

@section('content')
    <div class="box box-primary">
        @if($tasks)
            <div class="box-header ui-sortable-handle" style="cursor: move;">
                <i class="ion ion-clipboard"></i>

                <h3 class="box-title">@lang('tasks.heading')</h3>

                <div class="box-tools pull-right">
                    {{ $tasks->links() }}
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
                <ul class="todo-list ui-sortable">
                    @foreach($tasks as $task)
                        <li>
                            <span class="text">{{ $task->getAttribute('title') }}</span>
                            <!-- General tools such as edit or delete-->
                            <div class="tools">
                                <a href="{{ URL::route('tasks.edit', $task) }}"><i class="fa fa-edit"></i></a>
                                {{-- //@todo: need to override deleting approach --}}
                                <form method="post" action="{{ URL::route('tasks.destroy', $task) }}" class="inline">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-link" style="float:right;margin-left:5px;padding:0;"><i class="fa fa-trash-o"></i></button>
                                </form>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
                <a href="{{ URL::route('tasks.create') }}">
                    <button type="button" class="btn btn-default pull-right">
                        <i class="fa fa-plus"></i>
                        @lang('tasks.button.add')
                    </button>
                </a>
            </div>
        @endif
    </div>
@endsection
