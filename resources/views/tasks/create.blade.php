@extends('layouts.admin')

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('tasks.create')</h3>
        </div>
        <!-- /.box-header -->
        @includeWhen($errors->any(), 'helpers.validation_errors')
        <!-- form start -->
        <form role="form" method="post" action="{{ URL::route('tasks.store') }}">
            @csrf
            <div class="box-body">
                <div class="form-group">
                    <label for="title">@lang('tasks.form.title')</label>
                    <input name="title" class="form-control" type="text">
                </div>
                @if ($categories)
                    <div class="form-group">
                        <label for="category_id">@lang('tasks.form.category')</label>
                        <select name="category_id" class="form-control select2" multiple="multiple" data-placeholder="@lang('tasks.form.select')">
                            @foreach ($categories as $category)
                                <option value="{{ $category->getKey() }}">{{ $category->getAttribute('title') }}</option>
                            @endforeach
                        </select>
                    </div>
                @endif
                <div class="form-group">
                    <label for="due_date">@lang('tasks.form.dueDate')</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input name="due_date" type="text" class="form-control pull-right" id="datepicker">
                    </div>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">@lang('tasks.button.submit')</button>
            </div>
        </form>
    </div>
@endsection
