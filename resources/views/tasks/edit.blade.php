@extends('layouts.admin')

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('tasks.create')</h3>
        </div>
        <!-- /.box-header -->
        @includeWhen($errors->any(), 'helpers.validation_errors')
        <!-- form start -->
        <form role="form" method="post" action="{{ URL::route('tasks.update', $task) }}">
            @csrf
            @method('patch')
            <div class="box-body">
                <div class="form-group">
                    <label for="title">@lang('tasks.form.title')</label>
                    <input name="title" class="form-control" type="text" value="{{ $task->getAttribute('title') }}">
                </div>
                @if ($categories)
                    <div class="form-group">
                        <label for="category_id">@lang('tasks.form.category')</label>
                        <select name="category_id" class="form-control select2" multiple="multiple" data-placeholder="@lang('tasks.form.select')">
                            @foreach ($categories as $category)
                                <option value="{{ $category->getKey() }}"
                                    {{ ($task->getAttribute('category_id') === $category->getKey()) ? 'selected' : '' }}
                                >
                                    {{ $category->getAttribute('title') }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                @endif
                <div class="form-group">
                    <label for="due_date">@lang('tasks.form.dueDate')</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input name="due_date"
                               type="text"
                               class="form-control pull-right"
                               id="datepicker"
                               value="{{ $task->getAttribute('due_date') }}"
                        >
                    </div>
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="status" {{ ($task->getAttribute('status')) ? 'checked' : '' }}>
                            @lang('tasks.form.done')
                        </label>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">@lang('tasks.button.update')</button>
            </div>
        </form>
    </div>
@endsection
