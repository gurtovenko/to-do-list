<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Anything you want
    </div>
    <!-- Default to the left -->
    <strong>@lang('admin.footer.copyright') &copy; {{ date('Y') }} <a href="#">Company</a>.</strong>
    @lang('admin.footer.rights')
</footer>
