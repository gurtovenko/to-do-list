@extends('layouts.admin')

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('categories.create')</h3>
        </div>
        <!-- /.box-header -->
        @includeWhen($errors->any(), 'helpers.validation_errors')
        <!-- form start -->
        <form role="form" method="post" action="{{ URL::route('categories.store') }}">
            @csrf
            <div class="box-body">
                <div class="form-group">
                    <label for="title">@lang('categories.form.title')</label>
                    <input name="title" class="form-control" type="text">
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">@lang('categories.button.create')</button>
            </div>
        </form>
    </div>
@endsection
