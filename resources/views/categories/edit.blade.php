@extends('layouts.admin')

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('categories.edit')</h3>
        </div>
        <!-- /.box-header -->
        @includeWhen($errors->any(), 'helpers.validation_errors')
        <!-- form start -->
        <form role="form" method="post" action="{{ URL::route('categories.update', $category) }}">
            @csrf
            @method('patch')
            <div class="box-body">
                <div class="form-group">
                    <label for="title">@lang('categories.form.title')</label>
                    <input name="title" class="form-control" type="text" value="{{ $category->getAttribute('title') }}">
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">@lang('categories.button.update')</button>
            </div>
        </form>
    </div>
@endsection
