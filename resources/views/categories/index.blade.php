@extends('layouts.admin')

@section('content')
    <div class="box box-primary">
        @if($categories)
            <div class="box-header ui-sortable-handle" style="cursor: move;">
                <i class="ion ion-clipboard"></i>

                <h3 class="box-title">@lang('categories.heading')</h3>

                <div class="box-tools pull-right">
                    {{ $categories->links() }}
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
                <ul class="todo-list ui-sortable">
                    @foreach($categories as $category)
                        <li>
                            <span class="text">{{ $category->getAttribute('title') }}</span>
                            <!-- General tools such as edit or delete-->
                            <div class="tools">
                                <a href="{{ URL::route('categories.edit', $category) }}"><i class="fa fa-edit"></i></a>
                                {{-- //@todo: need to override deleting approach --}}
                                <form method="post" action="{{ URL::route('categories.destroy', $category) }}" class="inline">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-link" style="float:right;margin-left:5px;padding:0;"><i class="fa fa-trash-o"></i></button>
                                </form>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
                <a href="{{ URL::route('categories.create') }}">
                    <button type="button" class="btn btn-default pull-right">
                        <i class="fa fa-plus"></i>
                        @lang('tasks.button.add')
                    </button>
                </a>
            </div>
        @endif
    </div>
@endsection
