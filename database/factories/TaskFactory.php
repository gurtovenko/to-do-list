<?php

declare(strict_types = 1);

use App\Models\Task;
use App\Models\Category;
use Faker\Generator as Faker;

$factory->define(Task::class, function (Faker $faker) {
    return [
        'title' => $faker->unique()->words(rand(1, 5), true),
        'due_date' => $faker->dateTimeBetween('+1 day', '+1 week'),
        'category_id' => function () {
            return Category::inRandomOrder()->first()->getKey();
        },
    ];
});
