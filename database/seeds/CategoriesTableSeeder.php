<?php

declare(strict_types = 1);

use Illuminate\Database\Seeder;
use App\Models\Category;

/**
 * Class CategoriesTableSeeder
 */
class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        foreach ($this->getCategoriesList() as $category) {
            Category::create($category);
        }
    }

    /**
     * @return array
     */
    protected function getCategoriesList(): array
    {
        return json_decode(file_get_contents(__DIR__ . '/datafiles/categories.json'), true);
    }
}
