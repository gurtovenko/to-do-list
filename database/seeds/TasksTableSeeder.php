<?php

declare(strict_types = 1);

use Illuminate\Database\Seeder;
use App\Models\Task;

/**
 * Class TasksTableSeeder
 */
class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        factory(Task::class, 50)->create();
    }
}
